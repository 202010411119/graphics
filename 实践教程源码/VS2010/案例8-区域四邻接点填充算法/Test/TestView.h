
// TestView.h : CTestView 类的接口
//

#pragma once

#include "Line.h"//包含直线类
#include "StackNode.h"

class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	void ReadPoint();//读入顶点表
	void DrawGraph();//绘制图形
	void DrawPolygon(CDC *pDC);//绘制多边形
	void FillPolygon(CDC *pDC);//填充多边形
	void Push(CP2 point);//入栈
	void Pop(CP2 &point);//出栈
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CRect rect;//定义客户区
	COLORREF SeedClr;//种子色
	CP2 P[7];//点表
	BOOL bFill;//填充标志
	CStackNode *pHead,*pTop;//结点指针
	CP2 Seed,PointLeft,PointTop,PointRight,PointBottom;//种子及其四个邻接点
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDrawpic();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	virtual void OnInitialUpdate();
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

