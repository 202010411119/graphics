
// TestView.h : CTestView 类的接口
//

#pragma once

#include "Line.h"
#include "Face.h"
#include "Transform.h"

class CTestView : public CView
{
protected: // 仅从序列化创建
	CTestView();
	DECLARE_DYNCREATE(CTestView)

// 特性
public:
	CTestDoc* GetDocument() const;

// 操作
public:
	void ReadVertex();//读入顶点坐标
	void ReadFace();//读入面坐标
	void InitParameter();//变换参数
	void PerProject(CP3);//透视投影
	void DoubleBuffer(CDC *pDC);//双缓冲
	void DrawObject(CDC *);//绘制球面

// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	double R,Theta,Phi,d;//视点在用户坐标系中的球坐标
	double k[9];
	CP3 ViewPoint;//视点球坐标位置
	CP2 ScreenP;//屏幕坐标系的二维坐标点
	CP3 *V;//球面顶点一维数组
	CFace **F;//小面的二维数组
	int N1,N2;//N1为纬度区域,N2为经度区域
	double Alpha,Beta;//x方向旋转α角,y方向旋转β角
	BOOL bPlay;//动画开关
	CTransform tran;//变换对象
// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnDrawpic();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnPlay();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnUpdatePlay(CCmdUI *pCmdUI);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual void OnInitialUpdate();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // TestView.cpp 中的调试版本
inline CTestDoc* CTestView::GetDocument() const
   { return reinterpret_cast<CTestDoc*>(m_pDocument); }
#endif

