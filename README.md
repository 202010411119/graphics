# 计算机图形学

计算机图形学基础教程(VC++)第二版，计算机图形学实践教程(VC++)第二版 孔令德

![book.jpg](./img/book.jpg)

- 课程资料地址： [https://gitlab.com/zwdcdu/graphics](https://gitlab.com/zwdcdu/graphics)

## 学习工具

- VC++6.0：原始编程工具
- qt 5.15.2：高级编程工具
- git协议：用于提交实验报告
- git账号：每个同学必须有一个gitlab账号，并上传SSH公钥。
- VSCode: 实验报告编写

## 本书常用的自定义坐标系

- 视口坐标view，X向右，Y向下，在屏幕客户矩形区域的左上角，单位是像素。
- 窗口坐标window，方向自定义，单位也是自定义。本样例是：窗口坐标原点始终在屏幕中心，X轴向右，Y轴向上，与视口的比例是1:1。
- pDC对象绘制的时候采用的是**窗口坐标系**。默认情况窗口坐标和视口坐标相同。

```c
//自定义坐标系
CRect rect;
GetClientRect(&rect);
pDC->SetMapMode(MM_ANISOTROPIC);//设置映射模式
//改变视口坐标系
pDC->SetWindowExt(rect.Width(),rect.Height());//设置窗口
pDC->SetViewportExt(rect.Width(),-rect.Height());//设置视区:x轴水平向右，y轴垂直向上
//客户区中心为坐标系原点，rect.Width()/2,rect.Height()/2坐标是设备坐标系的。
pDC->SetViewportOrg(rect.Width()/2,rect.Height()/2);
```

![book.jpg](./img/user_coord.png)

## 参考

- [教材第一版本电子:书计算机图形学基础教程.pdf](./计算机图形学基础教程.pdf)
- 目录
  - “课程源码”目录，内置教材的所有源码
  - “实践教程源码”目录，内置教材的所有源码
  - PPT，内置教材PPT
  - vc_sample: VC高级样例
    - ![VC高级样例](./img/vc_sample.png)
  - qt_sample: QT高级样例
    - ![QT高级样例](./img/qt_sample.png)
- 开发工具
  - vc6_cn_full.rar是VC++6.0
  - qt-unified-windows-x86-4.0.1-online.exe是QT的安装程序。
- VC样例使用方法
  - 打开Visual C++ 6.0
  - 执行菜单“打开工作空间”
  - 选择工作空间文件（*.dsw）。VC自动打开包含的所有文件。
  - 打开后点击执行按钮（或者F5快捷键）。
- QT样例使用方法
  - 打开Qt
  - 执行菜单“打开文件或项目”
  - 选择*.pro文件。Qt自动打开包含的所有文件。
  - 执行菜单“构建->运行”（或者CTRL+R快捷键）
- 其他参考
- [VC控件与变量绑定小结](https://www.cnblogs.com/baogg/articles/1979299.html)
- [C++代码实现光线追踪-算法](http://www.kevinbeason.com/smallpt/)
- [MFC绘制犹他茶壶](https://blog.csdn.net/ha0ha0xuexi/article/details/117375424)
- [计算机图形学之Utah茶壶光照模型](https://www.bilibili.com/video/av88365991)
- [图形学视频（共11讲）](https://www.bilibili.com/video/BV1t7411J7dV?spm_id_from=333.999.0.0)
