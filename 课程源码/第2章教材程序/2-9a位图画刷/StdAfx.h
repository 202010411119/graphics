// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__BABF7A12_93D6_44B5_B941_D24602132B28__INCLUDED_)
#define AFX_STDAFX_H__BABF7A12_93D6_44B5_B941_D24602132B28__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
//功能：计算机图形学基础教程教学配套源程序
//作者：孔令德
//软件开发日期：2006-
//版权说明：本程序由太原工业学院孔令德开发，已经获得软件著作权
//本套代码于2012年获山西省教学成果一等奖，奖项名称为“计算机图形学实践教学资源库的建设”
//仅供读者学习孔令德编写的《计算机图形学基础教程（Visual C++版）》之用
//所有代码未经本人许可，禁止商业使用或者作为研究成果发表
//电话：0351-3566027

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__BABF7A12_93D6_44B5_B941_D24602132B28__INCLUDED_)
