// p3_2View.h : interface of the CP3_2View class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_P3_2VIEW_H__6710A8F8_7C40_4CD9_989A_2DC5FA2E3061__INCLUDED_)
#define AFX_P3_2VIEW_H__6710A8F8_7C40_4CD9_989A_2DC5FA2E3061__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CP3_2View : public CView
{
protected: // create from serialization only
	CP3_2View();
	DECLARE_DYNCREATE(CP3_2View)

// Attributes
public:
	CP3_2Doc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CP3_2View)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CP3_2View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CP3_2View)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in p3_2View.cpp
inline CP3_2Doc* CP3_2View::GetDocument()
   { return (CP3_2Doc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_P3_2VIEW_H__6710A8F8_7C40_4CD9_989A_2DC5FA2E3061__INCLUDED_)
