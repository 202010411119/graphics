// Circle.cpp: implementation of the CCircle class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "p3_9.h"
#include "Circle.h"
#include <math.h>
#define PI 3.1415926
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCircle::CCircle()
{

}

CCircle::~CCircle()
{

}
double Round(double val)
{
    return (val> 0.0) ? floor(val+ 0.5) : ceil(val- 0.5);
}
void settime(CDC *pDC,double m,double n,double r)
{
		CTime tm=CTime::GetCurrentTime(); 
	int hh=tm.GetHour(); //获取当前为几时
	int mm=tm.GetMinute(); //获取分钟
	int ss=tm.GetSecond(); //获取秒
	int hl=30;
	int ml=50;
	int sl=70;
	if(hh>12){
		hh=hh-12;
	}

	CPen N,*O;
	N.CreatePen(PS_SOLID,1,RGB(255,255,255));
	O=pDC->SelectObject(&N);

		//绘制时分秒针
		//时针
		pDC->MoveTo(m,n);
		pDC->LineTo(hl*cos((360-6*mm/60-30*hh-6*ss/60/60+90)*PI/180)+m,hl*sin((360-30*hh-6*ss/60/60-6*mm/60+90)*PI/180)+n);
		//分针
		pDC->MoveTo(m,n);
		pDC->LineTo(ml*cos((360-6*ss/60-6*mm+90)*PI/180)+m,ml*sin((360-6*mm+90-6*ss/60)*PI/180)+n);
		//秒针
		pDC->MoveTo(m,n);
		pDC->LineTo(sl*cos((360-6*ss+90)*PI/180)+m,sl*sin((360-6*ss+90)*PI/180)+n);

		pDC->SelectObject(O);
}
void CCircle::drawCirclep2(CDC * pDC,double m,double n,double r)//圆心
{
	double d = 1.25-r;
	int x = m,
	int y=Round(r);
	int s;
	//填充圆
	for(x=0;x<+r;x++)
	{
		for(s=-sqrt(r*r-x*x);s<sqrt(r*r-x*x);s++)
		{
			pDC->SetPixelV(x+m,s,RGB(255,255,255));
		}
	}
	for(x=0;x>-r;x--)
	{
		for(s=-sqrt(r*r-x*x);s<sqrt(r*r-x*x);s++)
		{
			pDC->SetPixelV(x+m,s,RGB(255,255,255));
		}
	}
	//绘制圆
	for(x = 0;x<Round(r/sqrt(2));x++)
	{

		pDC->SetPixelV(x+m,y+n,RGB(255,255,255));
		pDC->SetPixelV(y+m,x+n,RGB(255,255,255));
		pDC->SetPixelV(y+m,-x+n,RGB(255,255,255));
		pDC->SetPixelV(x+m,-y+n,RGB(255,255,255));
		pDC->SetPixelV(-x+m,-y+n,RGB(255,255,255));
		pDC->SetPixelV(-y+m,-x+n,RGB(255,255,255));
		pDC->SetPixelV(-y+m,x+n,RGB(255,255,255));
		pDC->SetPixelV(-x+m,y+n,RGB(255,255,255));
		if(d<0)
		{
			d=d+2*x+3;
		}
		else
		{
			d=d+2*(x-y)+5;
			y=y-1;
		}
	}
}
void CCircle::drawCirclep(CDC * pDC,double m,double n,double r)//走样圆
{
	double d = 1.25-r;
	int x = m,
	int y=Round(r);
	int s;
	pDC->SetTextColor(RGB(255,255,255));
	pDC->TextOut(m-20,n+r+30,"走样圆");
	//填充圆
	for(x=0;x<+r;x++)
	{
		for(s=-sqrt(r*r-x*x);s<sqrt(r*r-x*x);s++)
		{
			pDC->SetPixelV(x+m,s,RGB(0,0,0));
		}
	}
	for(x=0;x>-r;x--)
	{
		for(s=-sqrt(r*r-x*x);s<sqrt(r*r-x*x);s++)
		{
			pDC->SetPixelV(x+m,s,RGB(0,0,0));
		}
	}
	double deg=90;
	int flag=12;
	//绘制刻度
	pDC->SetBkMode(TRANSPARENT);
	while(flag!=0)
	{
		CString data;
		data.Format("%d",flag);
		pDC->SetTextColor(RGB(255,255,255));
		pDC->TextOut(m+(r-10)*cos(PI*deg/180)-5,n+(r-10)*sin(PI*deg/180)+7,data);
		deg=deg+30;
		flag--;
	}

	//绘制针
	settime(pDC, m, n,r);

	//绘制圆
	for(x = 0;x<Round(r/sqrt(2));x++)
	{

		pDC->SetPixelV(x+m,y+n,RGB(255,255,255));
		pDC->SetPixelV(y+m,x+n,RGB(255,255,255));
		pDC->SetPixelV(y+m,-x+n,RGB(255,255,255));
		pDC->SetPixelV(x+m,-y+n,RGB(255,255,255));
		pDC->SetPixelV(-x+m,-y+n,RGB(255,255,255));
		pDC->SetPixelV(-y+m,-x+n,RGB(255,255,255));
		pDC->SetPixelV(-y+m,x+n,RGB(255,255,255));
		pDC->SetPixelV(-x+m,y+n,RGB(255,255,255));
		if(d<0)
		{
			d=d+2*x+3;
		}
		else
		{
			d=d+2*(x-y)+5;
			y=y-1;
		}
	}
}
void CCircle::drawCircle(CDC * pDC,double m,double n,double r)
{

	int x = m;
	int y=Round(r);
	double deg=90;
	int flag=12;
	double e=0;
	int s;
	pDC->SetTextColor(RGB(255,255,255));
	pDC->TextOut(m-30,n+r+30,"反走样圆");
	//填充圆
	for(x=0;x<+r;x++)
	{
		for(s=-sqrt(r*r-x*x);s<sqrt(r*r-x*x);s++)
		{
			pDC->SetPixelV(x+m,s,RGB(0,0,0));
		}
	}
	for(x=0;x>-r;x--)
	{
		for(s=-sqrt(r*r-x*x);s<sqrt(r*r-x*x);s++)
		{
			pDC->SetPixelV(x+m,s,RGB(0,0,0));
		}
	}
	//绘制刻度
	pDC->SetBkMode(TRANSPARENT);
	while(flag!=0)
	{
		CString data;
		data.Format("%d",flag);
		pDC->SetTextColor(RGB(255,255,255));
		pDC->TextOut(m+(r-10)*cos(PI*deg/180)-5,n+(r-10)*sin(PI*deg/180)+7,data);
		deg=deg+30;
		flag--;
	}
	//绘制时刻 
    settime(pDC, m, n,r);

	//绘制圆
	for(x = 0;x<=Round(r/sqrt(2));x++)
	{		
		double ux=x;
		double uy=y;

		double dx=x;
		double dy=y-1;

		pDC->SetPixelV(Round(ux)+m,Round(uy)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(Round(dx)+m,Round(dy)+n,RGB(e*255,e*255,e*255));
	
		pDC->SetPixelV(Round(uy)+m,Round(ux)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(Round(dy)+m,Round(dx)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(Round(uy)+m,-Round(ux)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(Round(dy)+m,-Round(dx)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(Round(ux)+m,-Round(uy)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(Round(dx)+m,-Round(dy)+n,RGB(e*255,e*255,e*255));
		
		pDC->SetPixelV(-Round(ux)+m,-Round(uy)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(-Round(dx)+m,-Round(dy)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(-Round(uy)+m,-Round(ux)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(-Round(dy)+m,-Round(dx)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(-Round(uy)+m,Round(ux)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(-Round(dy)+m,Round(dx)+n,RGB(e*255,e*255,e*255));

		pDC->SetPixelV(-Round(ux)+m,Round(uy)+n,RGB((1-e)*255,(1-e)*255,(1-e)*255));
		pDC->SetPixelV(-Round(dx)+m,Round(dy)+n,RGB(e*255,e*255,e*255));
		

		e=e-(-x/sqrt(r*r-x*x));
		if(e>=1.0)
		{
			y=y-1;
			e=e-1;
		}
	}

}